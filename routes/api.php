<?php

Route::get('user', 'UserController@index');
Route::post('user/register', 'Api\AuthController@register');
Route::post('user/login', 'Api\AuthController@login');
Route::post('user/refresh', 'Api\AuthController@refresh');

Route::get('cliente', 'ClienteController@index');
Route::get('cliente/{id}', 'ClienteController@show');
Route::post('cliente', 'ClienteController@store');
Route::put('cliente/{id}', 'ClienteController@save');
Route::delete('cliente/{id}', 'ClienteController@destroy');

Route::get('compra', 'CompraController@index');
Route::get('compra/{id}', 'CompraController@show');
Route::post('compra', 'CompraController@store');
Route::put('compra/{id}', 'CompraController@save');
Route::delete('compra/{id}', 'CompraController@destroy');

Route::get('pago', 'PagoController@index');
Route::get('pago/{id}', 'PagoController@show');
Route::post('pago', 'PagoController@store');
Route::put('pago/{id}', 'PagoController@save');
Route::delete('pago/{id}', 'PagoController@destroy');