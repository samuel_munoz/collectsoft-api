<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $fillable = ['cliente_id', 'articulo', 'cantidad', 'comprado_en'];

    protected $dates = ['comprado_en'];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function pagos()
    {
        return $this->hasMany('App\Pago');
    }
}
