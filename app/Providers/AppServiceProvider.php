<?php

namespace App\Providers;

use App\Repositories\Cliente\ClienteInterface;
use App\Repositories\Cliente\ClienteRepository;
use App\Repositories\Compra\CompraInterface;
use App\Repositories\Compra\CompraRepository;
use App\Repositories\Pago\PagoInterface;
use App\Repositories\Pago\PagoRepository;
use App\Repositories\User\UserInterface;
use App\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ClienteInterface::class, ClienteRepository::class);
        $this->app->singleton(CompraInterface::class, CompraRepository::class);
        $this->app->singleton(PagoInterface::class, PagoRepository::class);
        $this->app->singleton(UserInterface::class, UserRepository::class);
    }
}
