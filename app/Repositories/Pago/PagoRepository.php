<?php
/**
 * Created by PhpStorm.
 * User: s_munozsd
 * Date: 05/03/2018
 * Time: 08:13 PM
 */

namespace App\Repositories\Pago;

use App\Pago;
use App\Repositories\AbstractRepository;

class PagoRepository extends AbstractRepository implements PagoInterface
{
    protected $model;

    public function __construct(Pago $model)
    {
        $this->model = $model;
    }
}