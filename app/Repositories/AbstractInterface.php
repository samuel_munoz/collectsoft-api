<?php
/**
 * Created by PhpStorm.
 * User: s_munozsd
 * Date: 05/03/2018
 * Time: 07:57 PM
 */

namespace App\Repositories;


interface AbstractInterface
{
    public function getAll();
    public function getById(int $id);
    public function create(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}