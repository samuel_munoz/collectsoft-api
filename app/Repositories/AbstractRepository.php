<?php
/**
 * Created by PhpStorm.
 * User: s_munozsd
 * Date: 05/03/2018
 * Time: 08:00 PM
 */

namespace App\Repositories;


class AbstractRepository implements AbstractInterface
{

    public function getAll()
    {
        return $this->model->all();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $data = $this->model->findOrFail($id);
        $data->update($attributes);
        return $data;
    }

    public function delete(int $id)
    {
        $this->getById($id)->delete();
        return true;
    }
}