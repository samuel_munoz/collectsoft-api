<?php
/**
 * Created by PhpStorm.
 * User: s_munozsd
 * Date: 05/03/2018
 * Time: 08:12 PM
 */

namespace App\Repositories\Compra;

use App\Compra;
use App\Repositories\AbstractRepository;

class CompraRepository extends AbstractRepository implements CompraInterface
{
    protected $model;

    public function __construct(Compra $model)
    {
        $this->model = $model;
    }
}