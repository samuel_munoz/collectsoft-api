<?php
/**
 * Created by PhpStorm.
 * User: s_munozsd
 * Date: 05/03/2018
 * Time: 08:15 PM
 */

namespace App\Repositories\User;

use App\Repositories\AbstractRepository;
use App\User;

class UserRepository extends AbstractRepository implements UserInterface
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }
}