<?php
/**
 * Created by PhpStorm.
 * User: s_munozsd
 * Date: 05/03/2018
 * Time: 08:09 PM
 */

namespace App\Repositories\Cliente;

use App\Cliente;
use App\Repositories\AbstractRepository;

class ClienteRepository extends AbstractRepository implements ClienteInterface
{
    protected $model;

    public function __construct(Cliente $model)
    {
        $this->model = $model;
    }
}