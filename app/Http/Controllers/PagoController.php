<?php

namespace App\Http\Controllers;

use App\Repositories\Pago\PagoInterface;
use Illuminate\Http\Request;

class PagoController extends Controller
{
    protected $pago;

    public function __construct(PagoInterface $pago)
    {
        $this->middleware('auth:api');
        $this->pago = $pago;
    }

    public function index()
    {
        return $this->pago->getAll();
    }

    public function show(int $id)
    {
        return $this->pago->getById($id);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'cliente_id' => 'required|bail',
            'compra_id' => 'required',
            'pagado' => 'required',
            'pendiente' => 'required'
        ]);
        return $this->pago->create($request->all());
    }

    public function save(Request $request, int $id)
    {
        $this->validate($request, [
            'cliente_id' => 'required|bail',
            'compra_id' => 'required',
            'pagado' => 'required',
            'pendiente' => 'required'
        ]);
        return $this->pago->update($id, $request->all());
    }

    public function destroy(int $id)
    {
        $this->pago->delete($id);
        return [];
    }
}
