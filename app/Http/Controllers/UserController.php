<?php

namespace App\Http\Controllers;

use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->middleware('auth:api');
        $this->user = $user;
    }

    public function index(Request $request)
    {
        return $request->user();
    }
}
