<?php

namespace App\Http\Controllers;

use App\Repositories\Cliente\ClienteInterface;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    protected $cliente;

    public function __construct(ClienteInterface $cliente)
    {
        $this->middleware('auth:api');
        $this->cliente = $cliente;
    }

    public function index()
    {
        return $this->cliente->getAll();
    }

    public function show(int $id)
    {
        return $this->cliente->getById($id);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|bail',
            'limite' => 'numeric'
        ]);
        return $this->cliente->create($request->all());
    }

    public function save(Request $request, int $id)
    {
        $this->validate($request, [
            'nombre' => 'required|bail',
            'limite' => 'numeric'
        ]);
        return $this->cliente->update($id, $request->all());
    }

    public function destroy(int $id)
    {
        $this->cliente->delete($id);
        return [];
    }
}
