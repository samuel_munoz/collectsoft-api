<?php

namespace App\Http\Controllers;

use App\Repositories\Compra\CompraInterface;
use Illuminate\Http\Request;

class CompraController extends Controller
{
    protected $compra;

    public function __construct(CompraInterface $compra)
    {
        $this->middleware('auth:api');
        $this->compra = $compra;
    }

    public function index()
    {
        return $this->compra->getAll();
    }

    public function show(int $id)
    {
        return $this->compra->getById($id);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'cliente_id' => 'required|bail',
            'articulo' => 'required|string',
            'cantidad' => 'required|numeric',
            'comprado_en' => 'required|date'
        ]);
        return $this->compra->create($request->all());
    }

    public function save(Request $request, int $id)
    {
        $this->validate($request, [
            'cliente_id' => 'required|bail',
            'articulo' => 'required|string',
            'cantidad' => 'required|numeric',
            'comprado_en' => 'required|date'
        ]);
        return $this->compra->update($id, $request->all());
    }

    public function destroy(int $id)
    {
        $this->compra->delete($id);
        return [];
    }
}
