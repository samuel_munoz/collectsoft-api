<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $fillable = ['cliente_id', 'compra_id', 'pagado', 'pendiente'];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function compra()
    {
        return $this->belongsTo('App\Compra');
    }
}
