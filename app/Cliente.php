<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['nombre', 'direccion', 'telefono', 'limite', 'adeudado'];

    public function compras()
    {
        return $this->hasMany('App\Compra');
    }

    public function pagos()
    {
        return $this->hasMany('App\Pago');
    }
}
